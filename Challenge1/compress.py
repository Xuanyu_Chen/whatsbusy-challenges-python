
def compress(in_str):
    length = len(in_str)
    if length < 1:
        return None
    cmp_str = ''
    counter = 0
    for index in range(0, length):
        element = in_str[index]
        if index == 0:
            cmp_str += element
            counter = 1
        else:
            if element == in_str[index-1]:
                counter += 1
            else:
                cmp_str += str(counter)
                cmp_str += element
                counter = 1
        if index == length - 1:
            cmp_str += str(counter)
    if len(cmp_str) >= length:
        return in_str
    return cmp_str

#Input your string here.
in_str = 'aaabbbcccaaa'
print(compress(in_str))
