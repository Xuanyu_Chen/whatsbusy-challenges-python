class Vertex(object):
    """docstring for Vertex"""
    def __init__(self, label, index):
        super(Vertex, self).__init__()
        self.label = label
        self.index = index

    def get_label(self):
        return self.label

    def get_index(self):
        return self.index


class Graph(object):
    """docstring for Graph"""
    def __init__(self):
        super(Graph, self).__init__()
        self.num_vertices = 0
        self.matrix = []
        self.vertex_list = {}

    def add_vertex(self, label):
        index = self.num_vertices
        new_vertex = Vertex(label, index)
        self.vertex_list[label] = new_vertex
        self.num_vertices += 1

        #Update the adjacent matrix
        adj_list = []
        for num in range(0, index):
            adj_list.append(0)
        self.matrix.append(adj_list)
        for num in range(0, self.num_vertices):
            self.matrix[num].append(0)
        return new_vertex

    def add_edge(self, from_label, to_label):
        if from_label not in self.vertex_list:
            self.add_vertex(from_label)
        if to_label not in self.vertex_list:
            self.add_vertex(to_label)
        from_v = self.vertex_list[from_label]
        to_v = self.vertex_list[to_label]
        self.matrix[from_v.get_index()][to_v.get_index()] = 1

    def get_vertices(self):
        return self.vertex_list.keys()


def identify_router(graph):
    max_vertex = None
    max_links, max_in = 0, 0
    for label in graph.vertex_list:
        vertex = graph.vertex_list[label]
        in_count, out_count = 0, 0
        index = vertex.get_index()
        for num in range(0, graph.num_vertices):
            if graph.matrix[num][index] == 1:
                in_count += 1
            if graph.matrix[index][num] == 1:
                out_count += 1
        if max_links < in_count + out_count:
            max_vertex = vertex
            max_links = in_count + out_count
            max_in = in_count
        elif max_links == in_count + out_count and max_in < in_count:
            max_vertex = vertex
            max_links = in_count + out_count
            max_in = in_count
    return max_vertex.get_label()

# Construct the input graph
graph = Graph()
graph.add_edge(2, 4)
graph.add_edge(4, 6)
graph.add_edge(6, 2)
graph.add_edge(2, 5)
graph.add_edge(5, 6)
# Test output
print(identify_router(graph))
