# Explanation

<p align='right'>Xuanyu Chen</p>



## Problem 1: String Compression

The running time of this compress function is **O(n)** since it iterates through the input string once with length of n.



## Problem 2: Network Failure Point

The running time of this identify_router function is **O($n^{2}$)**. The implementation is based on adjacent matrix, which takes **O(n)** to add a vertex and **O(1)** to add an edge. 

This function basically iterates through all the vertices(takes **O(n)** time) and for each vertex, check its corresponding row and column in the adjacent matrix to count the number of outbound links and inbound links, which takes another **O(n)** time. Thus, the overall running time of this function is **O($n^{2}$)**.